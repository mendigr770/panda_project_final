const express = require("express");
const app = express();
app.use(express.json())
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const port = process.env.PORT || 5000;

// Extended: https://swagger.io/specification/#infoObject
const swaggerOptions = {
  swaggerDefinition: {
    info: {
      version: "1.0.0",
      title: "Customer API",
      description: "Customer API Information",
      contact: {
        name: "Amazing Developer"
      },
      servers: ["http://localhost:5000"]
    }
  },
  // ['.routes/*.js']
  apis: ["app.js"]
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));


// Routes

/**
 * @swagger
 * /patients/locations/:lon/:lat/:radius:
 *    get:
 *      description: Use to return all patients filtered by landmark and radius
 *    parameters:
 *      - name: lon
 *        in: path
 *        description: value of lon
 *        required: true
 *        schema:
 *          type: integer
 *      - name: lat
 *        in: path
 *        description: value of lat
 *        required: true
 *        schema:
 *          type: integer
 *      - name: radius
 *        in: path
 *        description: value of lon
 *        required: true
 *        schema:
 *          type: integer
 *    responses:
 *      '201':
 *        description: Successfully got patient in landmark
 */
app.get('/patients/locations/:lon/:lat/:radius', (req, res) => {
    req.params; 
    res.json(req.params);
});
 
/**
 * @swagger
 * /patients/locations/polygon:
 *    get:
 *      description: Use to return all patients filtered by polygon
 *    parameters:
 *      - name: polygon
 *        in: path
 *        description: the polygon object
 *        required: true
 *        schema:
 *          type: object
 *    responses:
 *      '201':
 *        description: Successfully got patients in polygon
 */
app.get('/patients/locations/:polygon', (req, res) => {
    req.params; 
    res.json(req.params);
});
/**
 * @swagger
 * /patients/cities:
 *    get:
 *      description: Use to request patients counter per city
 *    responses:
 *      '200':
 *        description: A successful response
 */
app.get('/patients/cities', (req, res) => {
    res.send(`/patients/locations/cities`);
});
  
/**
 * @swagger
 * /patients/between/:startDate/:endDate:
 *    get:
 *      description: Use to request all patients between given dates
 *    parameters:
 *      - name: startDate
 *        in: path
 *        description: value of startDate
 *        required: true
 *        schema:
 *          type: integer
 *      - name: endDate
 *        in: path
 *        description: value of startDate
 *        required: true
 *        schema:
 *          type: integer
 *    responses:
 *      '200':
 *        description: A successful response
 */
app.get('/patients/between/:startDate/:endDate', (req, res) => {
    req.params;
    res.json(req.params);
});
  
/**
 * @swagger
 * /add:
 *    post:
 *      description: Use to add patient
 *    responses:
 *      '200':
 *        description: A successful response
 */
app.post('/add', (req, res) => {
   res.send("req.body")
});




app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});


